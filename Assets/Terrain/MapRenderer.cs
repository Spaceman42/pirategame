﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class MapRenderer : HeightMapOutput {


    public int tileResolution;
    
    public Color water;
    public Color sand;
    public Color grass;
    public Color rock;


    public void Start()
    {
       
    }
    
    public override void OnHeightMapUpdate()
    {
        BuildTexture();
    }


    private void BuildTexture()
    {
        HeightMap map = HeightMap;

        Texture2D tex = new Texture2D(map.size*tileResolution, map.size*tileResolution);

        for (int x = 0; x < tex.width; x++)
        {
            for (int y = 0; y < tex.height; y++)
            {
                float fx = x / (float)tileResolution;
                float fy = y / (float)tileResolution;

                int maxX = (int)Mathf.Ceil(fx);
                int minX = (int)Mathf.Floor(fx);
                int maxY = (int)Mathf.Ceil(fy);
                int minY = (int)Mathf.Floor(fy);

                float h1 = map.GetNormalizedValue(maxX, maxY);
                float d1 = DistanceScale(maxX, maxY, fx, fy);

                float h2 = map.GetNormalizedValue(minX, maxY);
                float d2 = DistanceScale(minX, maxY, fx, fy);

                float h3 = map.GetNormalizedValue(maxX, minY);
                float d3= DistanceScale(maxX, minY, fx, fy);

                float h4 = map.GetNormalizedValue(minX, minY);
                float d4 = DistanceScale(minX, minY, fx, fy);

                float distTotal = d1 + d2 + d3 + d4;

                h1 *= (d1 / distTotal);
                h2 *= (d2 / distTotal);
                h3 *= (d3 / distTotal);
                h4 *= (d4 / distTotal);

                tex.SetPixel(x, y, getColor((h1 + h2 + h3 + h4)/4.0f));
            }
        }
        tex.filterMode = FilterMode.Point;
        tex.Apply();
        MeshRenderer r = GetComponent<MeshRenderer>();
        r.material.SetTexture(0, tex);
    }


    private float DistanceScale(int ix, int iy, float fx, float fy)
    {
        return new Vector2(ix - fx, iy - fy).sqrMagnitude;
    }

    private Color getColor(float height)
    {
        if (height < 0.5f)
        {
            return water;
        }
        else if (height < 0.6f)
        {
            return sand;
        }
        return grass;
    }

    private float Average(float a, float b, float c, float d)
    {
        return (a + b + c + d) / 4.0f;
    }
}
