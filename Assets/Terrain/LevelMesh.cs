﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class LevelMesh : HeightMapOutput {

    private HeightMap map;
    
    private float tileSize;

    public int tileResolution;
    public int terrainResoltion;
    
    // Use this for initialization
	void Start () {
        tileSize = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnHeightMapUpdate()
    {
        //throw new System.NotImplementedException();
        BuildMesh();
    }

    private void BuildTexture()
    {
        
    } 

    public void BuildMesh()
    {
        map = HeightMap;
        int numTiles = map.size * map.size;
        int numTris = numTiles * 2;

        int vSize = map.size + 1;

        int numVerts = vSize * vSize;


        Vector3[] verts = new Vector3[numVerts];
        Vector3[] norms = new Vector3[numVerts];
        Vector2[] uv = new Vector2[numVerts];

        int[] triangles = new int[numTris * 3];


        for (int x = 0; x < map.size; x++)
        {
            for (int y = 0; y < map.size; y++)
            {
                int i = y * vSize + x;
                verts[i] = new Vector3(x*tileSize, y*tileSize, 0);
                norms[i] = Vector3.up;
                uv[i] = new Vector2((float)x / map.size, (float)y / map.size);
            }
        }

        for (int x = 0; x < map.size; x++)
        {
            for (int y = 0; y < map.size; y++)
            {
                int i = y * map.size + x;
                int tri = i * 6;
                triangles[tri + 0] = y * vSize + x + 0;
                triangles[tri + 1] = y * vSize + x + vSize + 1;
                triangles[tri + 2] = y * vSize + x + vSize + 0;

                triangles[tri + 3] = y * vSize + x + 0;
                triangles[tri + 4] = y * vSize + x + 1;
                triangles[tri + 5] = y * vSize + x + vSize + 0;
            }
        }


        Mesh mesh = new Mesh();
        mesh.vertices = verts;
        mesh.normals = norms;
        mesh.uv = uv;
        mesh.triangles = triangles;

        MeshFilter meshFilter = GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        MeshCollider meshCollider = GetComponent<MeshCollider>();

        meshFilter.mesh = mesh;
    }
}
