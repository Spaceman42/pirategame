﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Terrain))]
[RequireComponent(typeof(HeightMap))]

public class TerrainRenderer : HeightMapOutput {

    public override void OnHeightMapUpdate()
    {
        Terrain t = GetComponent<Terrain>();
        HeightMap map = GetComponent<HeightMap>();

        TerrainData td = new TerrainData();
        td.heightmapResolution = map.Size;
        float[,] data = map.AsFloatArray();

        for (int x = 0; x < map.size; x++)
        {
            for (int y = 0; y < map.size; y++)
            {
                data[x, y] = ConvertToUnitRange(data[x, y]/15.0f, map.Min, map.Max);
            }
        }
        td.SetHeights(0, 0, data);
        t.terrainData = td;
    }

    private float ConvertToUnitRange(float value, float min, float max)
    {
        float newValue = (value - min) / (max - min);
        return newValue;
    }
	
}
