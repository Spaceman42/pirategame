﻿using UnityEngine;
using System.Collections;

public class DebugHeightMapRenderer : HeightMapOutput {

	public bool draw = false;

	private Texture2D tex;

	public override void OnHeightMapUpdate ()
	{
		HeightMap map = HeightMap;
		tex = new Texture2D (map.Size, map.Size);

		for (int x = 0; x < map.Size; x++) {
			for (int y = 0; y < map.Size; y++) {
				float val = ConvertToUnitRange(map.GetValue (x, y), map.Min, map.Max);
				tex.SetPixel(x, y, new Color(val, val, val, 1));
			}
		}

        tex.Apply();
		draw = true;
				         
	}


	private float ConvertToUnitRange(float value, float min, float max) {
		float newValue = (value - min) / (max - min);
		return newValue;
	}


	public void OnGUI() {
		if (draw) {
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), tex);
		}
	}
}
