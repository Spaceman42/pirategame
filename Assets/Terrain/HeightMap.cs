﻿using UnityEngine;
using System.Collections;
[DisallowMultipleComponent]
public class HeightMap : MonoBehaviour {
	public float[]	data;
	public int size;

	private float min;
	private float max;

	private bool initialized;

	public bool start;

	public void Start() {
		data = new float[size * size];
		GetComponent<HeightMapGenerator> ().GenerateMap ();
	}

	public void Init() {
		FindAndSetMinMax (data);

		initialized = true;
		UpdateOutputs ();
	}

	private void UpdateOutputs() {
		foreach (HeightMapOutput o in GetComponents<HeightMapOutput>()) {
			o.OnHeightMapUpdate();
		}
	}

	private void FindAndSetMinMax(float[] data)
	{
		float min = float.MaxValue;
		float max = float.MinValue;
		foreach (float f in data) {
			if (f < min) {
				min = f;
			}
			if (f > max) {
				max = f;
			}
		}

		this.min = min;
		this.max = max;
	}

	public void SetValue(int x, int y, float value) {
		data[x + size*y] = value;
	}
	
	public float GetValue(int x, int y) {
		if (x < 0) {
			x = 0;
		} else if (x >= size) {
			x = size - 1;
		}
		if (y < 0) {
			y = 0;
		} else if (y >= size) {
			y = size -1;
		}
		if (x + size * y > data.Length - 1 || x + size * y < 0) {
			Debug.LogError("Invalid x, y request from heightmap");
		}
		//float value = data[x + size * y];

		return data[x + size * y];
	}

    public float GetNormalizedValue(int x, int y)
    {
        return ConvertToUnitRange(GetValue(x, y), min, max);
    }

	public float[] Data {
		get {
			return this.data;
		}
	}

	public int Size {
		get {
			return this.size;
		}
	}

	public float Min {
		get {
			return this.min;
		}
	}

	public float Max {
		get {
			return this.max;
		}
	}

	public bool Initialized {
		get {
			return this.initialized;
		}
	}

    public float[,] AsFloatArray()
    {
        float[,] array = new float[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                array[x, y] = GetValue(x, y);
            }
        }

        return array;
    }

    private float ConvertToUnitRange(float value, float min, float max)
    {
        float newValue = (value - min) / (max - min);
        return newValue;
    }
}

