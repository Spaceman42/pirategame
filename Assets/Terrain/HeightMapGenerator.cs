﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(HeightMap))]
public abstract class HeightMapGenerator : MonoBehaviour {


	protected HeightMap HeightMap {
		get {
			return GetComponent<HeightMap>();
		}
	}

	public abstract void GenerateMap();

	protected float NextFloat() {
		return Random.Range (0.0f, 1.0f);
	}
}
