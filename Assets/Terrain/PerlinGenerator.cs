﻿using UnityEngine;
using System.Collections;

public class PerlinGenerator : HeightMapGenerator {

    public int octaves;
    public float persistance;
    public float amplitude;
    public float frequency;


    private float Noise(int x, int y) 
    {
        int n = x + y * 57;
        n = (n << 13) ^ n;
        return ( 1.0f - ( (n * (n * n * 15731 + 789221) + 1376312589)) / 1073741824.0f);
    }


    private float SmoothedNoise(int x, int y)
    {
        float corners = (Noise(x - 1, y - 1) + Noise(x + 1, y - 1) + Noise(x - 1, y + 1) + Noise(x + 1, y + 1)) / 16;
        float sides = (Noise(x - 1, y) + Noise(x + 1, y) + Noise(x, y - 1) + Noise(x, y + 1)) / 8;
        float center = Noise(x, y) / 4;
        return corners + sides + center;
    }

    private float InterpolatedNoise(float x, float y) 
    {
        int ix = (int)x;
        float fracX = x - (float)ix;

        int iy = (int)y;
        float fracY = y - (float)iy;

        float v1 = SmoothedNoise(ix, iy);
        float v2 = SmoothedNoise(ix + 1, iy);
        float v3 = SmoothedNoise(ix, iy + 1);
        float v4 = SmoothedNoise(ix + 1, iy + 1);

        float i1 = Interpolate(v1, v2, fracX);
        float i2 = Interpolate(v3, v4, fracY);

        return Interpolate(i1, i2, fracY);
    }

    private float Interpolate(float x, float y, float a)
    {
        float negA = 1.0f - a;
        float negA2 = negA * negA;
        float fac1 = 3.0f * negA2 - 2.0f * (negA2 * negA);

        float a2 = a * a;
        float fac2 = 3.0f * a2 - 2.0f * (a2 * a);

        return x * fac1 + y * fac2;
    }

    public float PerlinNoise2D(float x, float y)
    {
        float total = 0;
        float p = persistance;
        float n = octaves;
        
        for (int i = 0; i < n; i++)
        {
            float frequency = Mathf.Pow(2, i);
            float amplitude = Mathf.Pow(p, i);

            //total = total + InterpolatedNoise(x * frequency, y * frequency) * amplitude;
        } 

        for (int i = 0; i < n; i++)
        {
            total += InterpolatedNoise(x * frequency * i, y * frequency * i)*Mathf.Pow(p, i);
            float island = InterpolatedNoise(x * frequency * 350.0f + i, y * frequency * 350.0f * i) * 0.3f;
            if (island > 1.0f/2.0f)
            {
                total += island;
            }
        }
        return total/n;
    }
    
    
   public override void GenerateMap()
   {
       HeightMap map = GetComponent<HeightMap>();

       for (int x = 0; x < map.Size; x++)
       {
           for (int y = 0; y < map.Size; y++)
           {
               map.SetValue(x, y, PerlinNoise2D(x, y));
           }
       }

       map.Init();
   }
}
