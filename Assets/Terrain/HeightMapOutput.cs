﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(HeightMap))]
public abstract class HeightMapOutput : MonoBehaviour {
	protected HeightMap HeightMap {
		get {
			return GetComponent<HeightMap>();
		}
	}

	public abstract void OnHeightMapUpdate();
}
